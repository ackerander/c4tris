#include <SDL2/SDL.h>
#include <limits.h>
#include "game.h"

#define NAME "C4tris"
#define BG 0x10, 0x1A, 0x1D, 0xFF 
#define GRID_COLOUR 0xE0, 0xE0, 0xE0
#define DASINIT 250
#define DASCONT 25
#define DROPSPEED 1000

#define MIN(a, b) ((a) < (b) ? (a) : (b))

enum codes { SUCCESS, INIT_SDL, RENDERER, WINDOW };
static const uint8_t colours[8 * 3] = { 0x80, 0x80, 0x80,
					0x00, 0xFF, 0xFF,
					0x00, 0x00, 0xFF,
					0xFF, 0xAA, 0x00,
					0xFF, 0xFF, 0x00,
					0x00, 0xFF, 0x00,
					0x99, 0x00, 0xFF,
					0xFF, 0x00, 0x00 };

extern game_t game;

static SDL_Renderer *renderer;
static SDL_Window *window;
static uint16_t size;
static uint16_t originX;
static uint16_t originY;

void
renderTile(uint8_t row, uint8_t col)
{
	SDL_Rect renderQuad = { originX + size * col,
			 originY + size * row, size,  size };
	uint8_t i = 3 * game.board[row][col];

	SDL_SetRenderDrawColor(renderer,
			colours[i], colours[i + 1], colours[i + 2], 0xFF);
	SDL_RenderFillRect(renderer, &renderQuad);

	if (!i) {
		renderQuad.x += 1;
		renderQuad.y += 1;
		renderQuad.w = renderQuad.h = size - 2;
		SDL_SetRenderDrawColor(renderer, GRID_COLOUR, 0xFF);
		SDL_RenderDrawRect(renderer, &renderQuad);
	}
}

void
renderPreview(int x, int y, uint8_t piece)
{
	SDL_Rect renderQuad = { 0, 0, size, size };

	if (!piece)
		return;
	SDL_SetRenderDrawColor(renderer, colours[3 * piece],
			colours[3 * piece + 1], colours[3 * piece + 2], 0xFF);

	--piece;
	for (int i = 0; i < 8; i += 2) {
		renderQuad.x = x + size * pieceCord[piece][i + 1];
		renderQuad.y = y + size * pieceCord[piece][i];
		SDL_RenderFillRect(renderer, &renderQuad);
	}
}

void
renderPiece()
{
	SDL_Rect renderQuad = { 0, 0, size, size };

	if (!game.queue[game.qHead])
		return;
	SDL_SetRenderDrawColor(renderer,
		colours[3 * game.queue[game.qHead]],
		colours[3 * game.queue[game.qHead] + 1],
		colours[3 * game.queue[game.qHead] + 2], 0xFF);
	for (int i = 0; i < 4; ++i) {
		if (game.currPiece[i][0] >= 0) {
			renderQuad.y = originY + size * game.currPiece[i][0];
			renderQuad.x = originX + size * game.currPiece[i][1];
			SDL_RenderFillRect(renderer, &renderQuad);
		}
	}
}

void
renderGhost()
{
	SDL_Rect renderQuad = { 0, 0, size, size };

	if (!game.queue[game.qHead] || game.dropOffset < 0)
		return;
	SDL_SetRenderDrawColor(renderer,
		colours[3 * game.queue[game.qHead]],
		colours[3 * game.queue[game.qHead] + 1],
		colours[3 * game.queue[game.qHead] + 2], 0x40);
	for (int i = 0; i < 4; ++i) {
		if (game.currPiece[i][0] + game.dropOffset >= 0) {
			renderQuad.y = originY + size * (game.currPiece[i][0] + game.dropOffset);
			renderQuad.x = originX + size * game.currPiece[i][1];
			SDL_RenderFillRect(renderer, &renderQuad);
		}
	}
}

void
renderQueue()
{
	int x = originX + size * (GAP + BOARD_W), y = originY;

	for (int i = 1; i <= PREVIEW; ++i) {
		renderPreview(x, y, QUEUE_ELEM(i));
		y += 4 * size;
	}
}

void
renderGame()
{
	SDL_SetRenderDrawColor(renderer, BG);
	SDL_RenderClear(renderer);
	/* RenderBoard */
	for (int i = 0; i < BOARD_H; ++i) {
		for (int j = 0; j < BOARD_W; ++j)
			renderTile(i, j);
	}
	renderPiece();
	renderGhost();
	renderQueue();
	renderPreview(originX - size * (GAP + 4), originY, game.hold);
}

void
gameLoop()
{
	SDL_Event e;
	Uint32 t1, t2, tDrop = 0;
	int dasStates[3] = {0, INT_MIN, INT_MIN};

	newGame();
	t1 = SDL_GetTicks();
start:
	t2 = SDL_GetTicks();
	t1 = t2 - t1;
	if (t1 < 20)
		SDL_Delay(20 - t1);
	t1 = t2;
	renderGame();
	SDL_RenderPresent(renderer);
	while (SDL_WaitEvent(&e)) {
		switch (e.type) {
		case SDL_QUIT:
			return;
		case SDL_KEYDOWN:
			if (e.key.keysym.sym == SDLK_SPACE) {
				genTetromino();
				goto loop;
			}
		}
	}
	goto start;
loop:
	t2 = SDL_GetTicks();
	t1 = t2 - t1;
	if (t1 < 20)
		SDL_Delay(20 - t1);
	renderGame();
	SDL_RenderPresent(renderer);
	while (SDL_PollEvent(&e)) {
		switch (e.type) {
		case SDL_QUIT:
			return;
		case SDL_KEYDOWN:
			switch (e.key.keysym.sym) {
			case SDLK_z:
				rotate(1);
				break;
			case SDLK_x:
				rotate(-1);
				break;
			case SDLK_c:
				swap();
				tDrop = 0;
				break;
			case SDLK_SPACE:
				drop();
				tDrop = 0;
				genTetromino();
			}
		}
	}
	const Uint8 *keyStates = SDL_GetKeyboardState(0);
	if (keyStates[SDL_SCANCODE_DOWN]) {
		if (dasStates[0] >= DASCONT) {
			move(1, 0);
			tDrop = 0;
			dasStates[0] -= DASCONT;
		}
		dasStates[0] += t1;
	} else
		dasStates[0] = 0;

	if (keyStates[SDL_SCANCODE_LEFT]) {
		if (dasStates[1] == INT_MIN) {
			move(0, -1);
			dasStates[1] = -t1;
		} else if (dasStates[1] >= DASCONT) {
			move(0, -1);
			dasStates[1] += t1 - DASCONT;
		} else if (-dasStates[1] >= DASINIT) {
			move(0, -1);
			dasStates[1] = t1 - dasStates[1] - DASINIT;
		} else
			dasStates[1] += dasStates[1] < 0 ? -t1 : t1;
	} else
		dasStates[1] = INT_MIN;

	if (keyStates[SDL_SCANCODE_RIGHT]) {
		if (dasStates[2] == INT_MIN) {
			move(0, 1);
			dasStates[2] = -t1;
		} else if (dasStates[2] >= DASCONT) {
			move(0, 1);
			dasStates[2] += t1 - DASCONT;
		} else if (-dasStates[2] >= DASINIT) {
			move(0, 1);
			dasStates[2] = t1 - dasStates[2] - DASINIT;
		} else
			dasStates[2] += dasStates[2] < 0 ? -t1 : t1;
	} else
		dasStates[2] = INT_MIN;

	if ((tDrop += t1) >= DROPSPEED) {
		move(1, 0);
		tDrop -= DROPSPEED;
	}

	t1 = t2;
	goto loop;
}

uint8_t
init()
{
	int w, h;
	uint8_t scaleX, scaleY;

	/* Init SDL */
	if (SDL_Init(SDL_INIT_VIDEO))
		return INIT_SDL;
	/* Create window */
	window = SDL_CreateWindow(NAME,
			SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 0, 0,
			SDL_WINDOW_FULLSCREEN_DESKTOP);
	if (!window)
		return WINDOW;
	/* Create renderer */
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if (!renderer)
		return RENDERER;
	/* Blending */
	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
	/* Scaling */
	SDL_GetWindowSize(window, &w, &h);
	scaleX = w / BOARD_W >> 1;
	scaleY = h / BOARD_H >> 1;
	size = MIN(scaleX, scaleY);
	originX = (w - size * BOARD_W) >> 1;
	originY = (h - size * BOARD_H) >> 1;
	return SUCCESS;
}

void
quit(uint8_t code)
{
	switch (code) {
	case SUCCESS:
		SDL_DestroyRenderer(renderer);
		/* FALLTHRU */
	case RENDERER:
		SDL_DestroyWindow(window);
		/* FALLTHRU */
	case WINDOW:
		SDL_Quit();
		/* FALLTHRU */
	default:
		exit(code);
	}
}
