#ifndef GAME_H
#define GAME_H

#include <stdint.h>

#define BOARD_H 20
#define BOARD_W 10
#define PREVIEW 5
#define QUEUE_SZ (PREVIEW + 7)
#define GAP 1

#define QUEUE_ELEM(i) game.queue[(game.qHead + (i)) % QUEUE_SZ]

enum tetrominos { NONE = 0, I, J, L, O, S, T, Z };

static const uint8_t pieceCord[7][8] = {{1, 0, 1, 1, 1, 2, 1, 3},
					{1, 1, 1, 2, 0, 0, 1, 0},
					{1, 1, 1, 2, 0, 2, 1, 0},
					{1, 1, 1, 2, 0, 1, 0, 2},
					{1, 1, 0, 2, 0, 1, 1, 0},
					{1, 1, 1, 2, 0, 1, 1, 0},
					{1, 1, 1, 2, 0, 1, 0, 0}};

typedef struct {
	int8_t currPiece[4][2];
	uint8_t board[BOARD_H][BOARD_W];
	uint8_t queue[QUEUE_SZ];
	uint8_t currRot;
	int8_t dropOffset;
	int8_t qHead;
	int8_t qTail;
	uint8_t hold;
} game_t;

void newGame();
uint8_t move(int8_t x, int8_t y);
uint8_t rotate(int8_t rotation);
void swap();
void drop();
void clear();
void genTetromino();

uint8_t init();
void renderBoard();
void renderQueue();
void renderHold();
void renderPiece();
void gameLoop();
void quit(uint8_t code);

#endif
