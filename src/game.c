#include "game.h"
#include <stdlib.h>
#include <time.h>
#include <string.h>

struct tree {
	struct tree *a, *b;
	uint8_t n;
};

game_t game;

struct tree **
insert(struct tree **tr, uint8_t data)
{
	if (!*tr || (*tr)->n == data)
		return tr;
	else
		return insert(data > (*tr)->n ? &(*tr)->a : &(*tr)->b , data);
}

void
toArr(struct tree *tr, uint8_t arr[], uint8_t *len)
{
	if (tr) {
		toArr(tr->a, arr, len);
		arr[(*len)++] = tr->n;
		toArr(tr->b, arr, len);
		free(tr);
	}
}

uint8_t
queueLen()
{
	return game.qTail < 0 ? 0 :
		game.qTail - game.qHead + (game.qHead > game.qTail) * QUEUE_SZ + 1;
}

void
drop()
{
	struct tree *tr = 0, **tmp;
	uint8_t arr[4], len = 0, j, offset;

	for (int i = 0; i < 4; ++i)
		game.board[game.currPiece[i][0] += game.dropOffset][game.currPiece[i][1]]
			= game.queue[game.qHead];
	for (int i = 0; i < 4; ++i) {
		if (!*(tmp = insert(&tr, game.currPiece[i][0]))) {
			for (j = 0; j < BOARD_W && game.board[game.currPiece[i][0]][j]; ++j);
			if (j == BOARD_W) {
				*tmp = malloc(sizeof(struct tree));
				(*tmp)->a = (*tmp)->b = 0;
				(*tmp)->n = game.currPiece[i][0];
			}
		}
	}
	toArr(tr, arr, &len);
	if (len)
		offset = arr[0];
	for (int i = 0; i < len - 1; ++i) {
		offset -= arr[i] - arr[i + 1] - 1;
		memmove(game.board[offset + 1], game.board[arr[i + 1] + 1],
			BOARD_W * (arr[i] - arr[i + 1] - 1));
	}
	if (len) {
		memmove(game.board[offset -= arr[len - 1] - 1], game.board[0],
			BOARD_W * arr[len - 1]);
		memset(game.board[0], 0, BOARD_W * offset);
	}
}

uint8_t
empty(int8_t row, int8_t col)
{
	return col < BOARD_W && row < BOARD_H && col >= 0 && (row < 0 ||
		(row >= 0 && !game.board[row][col]));
}

void
dropOffset()
{
	int8_t i, row;
	for (i = 0; ; ++i) {
		for (int j = 0; j < 4; ++j) {
			row = game.currPiece[j][0] + i;
			if (row >= BOARD_H || (row >= 0 &&
				game.board[row][game.currPiece[j][1]]))
				goto end;
		}
	}
end:
	game.dropOffset = i - 1;
}

uint8_t
move(int8_t row, int8_t col)
{
	for (int i = 0; i < 4; ++i) {
		if (!empty(game.currPiece[i][0] + row, game.currPiece[i][1] + col))
			return 0;
	}
	for (int i = 0; i < 4; ++i) {
		game.currPiece[i][0] += row;
		game.currPiece[i][1] += col;
	}
	if (col)
		dropOffset();
	game.dropOffset -= row;
	return 1;
}

uint8_t
rot(int8_t rotation)
{
	static const int8_t turnTable[8][10] = {{0, 0, 0, -1, -1, -1,  2,  0,  2, -1},
						{0, 0, 0,  1, -1,  1,  2,  0,  2,  1},
						{0, 0, 0, -1,  1, -1, -2,  0, -2, -1},
						{0, 0, 0, -1,  1, -1, -2,  0, -2, -1},
						{0, 0, 0, -1, -1, -1,  2,  0,  2, -1},
						{0, 0, 0,  1, -1,  1,  2,  0,  2,  1},
						{0, 0, 0,  1,  1,  1, -2,  0, -2,  1}, 
						{0, 0, 0,  1,  1,  1, -2,  0, -2,  1}};
	uint8_t tmp[4][2], tableIdx = 2*game.currRot + (rotation + 1)/2, test, i;

	memcpy(tmp, game.currPiece, sizeof(tmp));
	for (int i = 1; i < 4; ++i) {
		game.currPiece[i][0] = (tmp[0][1] - tmp[i][1]) * rotation + tmp[0][0];
		game.currPiece[i][1] = (tmp[i][0] - tmp[0][0]) * rotation + tmp[0][1];
	}
	for (test = 0; test < 5; ++test) {
		for (i = 0; i < 4 &&
		empty(game.currPiece[i][0] + turnTable[tableIdx][2*test],
		game.currPiece[i][1] + turnTable[tableIdx][2*test + 1]); ++i);
		if (i == 4)
			goto place;
	}
	memcpy(game.currPiece, tmp, sizeof(tmp));
	return 0;
place:
	for (i = 0; i < 4; ++i) {
		game.currPiece[i][0] += turnTable[tableIdx][2*test];
		game.currPiece[i][1] += turnTable[tableIdx][2*test + 1];
	}
	game.currRot = (4 + game.currRot + rotation) % 4;
	dropOffset();
	return 1;
}

uint8_t
rotI(int8_t rotation)
{
	static const int8_t iTable[8][10] = {{ 0, 0,  0, -2,  0,  1,  1, -2, -2,  1},
					     { 0, 0,  0, -1,  0,  2, -2, -1,  1,  2},
					     { 0, 0,  0,  1,  0, -2,  2,  1, -1, -2},
					     { 0, 0,  0, -2,  0,  1,  1, -2, -2,  1},
					     { 0, 0,  0,  2,  0, -1, -1,  2,  2, -1},
					     { 0, 0,  0,  1,  0, -2,  2,  1, -1, -2},
					     { 0, 0,  0, -1,  0,  2, -2, -1,  1,  2},
					     { 0, 0,  0,  2,  0, -1, -1,  2,  2, -1}};
	uint8_t tmp[4][2], tableIdx = 2*game.currRot + (rotation + 1)/2, test, i;
	float centreR = game.currPiece[1][0] - 0.5 + (!game.currRot || game.currRot == 3),
	      centreC = game.currPiece[1][1] - 0.5 + (game.currRot < 2);

	memcpy(tmp, game.currPiece, sizeof(tmp));
	for (int i = 0; i < 4; ++i) {
		game.currPiece[i][0] = (centreC - tmp[i][1]) * rotation + centreR;
		game.currPiece[i][1] = (tmp[i][0] - centreR) * rotation + centreC;
	}
	for (test = 0; test < 5; ++test) {
		for (i = 0; i < 4 &&
		empty(game.currPiece[i][0] + iTable[tableIdx][2*test],
		game.currPiece[i][1] + iTable[tableIdx][2*test + 1]); ++i);
		if (i == 4)
			goto place;
	}
	memcpy(game.currPiece, tmp, sizeof(tmp));
	return 0;
place:
	for (i = 0; i < 4; ++i) {
		game.currPiece[i][0] += iTable[tableIdx][2*test];
		game.currPiece[i][1] += iTable[tableIdx][2*test + 1];
	}
	game.currRot = (4 + game.currRot + rotation) % 4;
	dropOffset();
	return 1;
}

uint8_t
rotate(int8_t rotation)
{
	if (rotation != -1 && rotation != 1)
		return 0;
	switch (game.queue[game.qHead]) {
	case O:
		return 1;
	case I:
		return rotI(rotation);
	default:
		return rot(rotation);
	}
}

void
genPieces()
{
	uint8_t tmp, randIdx;
	int start;
	
	if (queueLen() > QUEUE_SZ - 7)
		return;
	start = game.qTail < 0 ? game.qHead + !game.qHead * QUEUE_SZ : game.qTail;
	game.qTail = (start + 7) % QUEUE_SZ;
	for (int i = 1; i < 8; ++i)
		game.queue[(start + i) % QUEUE_SZ] = i;
	for (int i = 7; i > 1; --i) {
		randIdx = (start + (rand() % i) + 1) % QUEUE_SZ;
		tmp = game.queue[(start + i) % QUEUE_SZ];
		game.queue[(start + i) % QUEUE_SZ] = game.queue[randIdx];
		game.queue[randIdx] = tmp;
	}
}

void
genTetromino()
{
	uint8_t piece;

	if (game.qHead == game.qTail)
		return;
	game.qHead = (game.qHead + 1) % QUEUE_SZ;
	piece = game.queue[game.qHead] - 1;

	for (int i = 0; i < 4; ++i) {
		game.currPiece[i][0] = pieceCord[piece][2*i] - 1;
		game.currPiece[i][1] = pieceCord[piece][2*i + 1] + 3;
	}
	game.currRot = 0;
	dropOffset();
	genPieces();
}

void
swap()
{
	uint8_t tmp = game.hold;

	game.hold = game.queue[game.qHead];
	game.queue[game.qHead] = tmp;
	for (int i = 0; i < 4; ++i) {
		game.currPiece[i][0] = pieceCord[tmp - 1][2*i] - 1;
		game.currPiece[i][1] = pieceCord[tmp - 1][2*i + 1] + 3;
	}
	if (!tmp)
		genTetromino();
	else
		dropOffset();
}

void
newGame()
{
	srand(time(NULL));
	memset(game.board, 0, sizeof(game.board));
	game.hold = NONE;
	game.dropOffset = -1;
	game.qHead = 0;
	game.qTail = -1;
	genPieces();
}
