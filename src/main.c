#include "game.h"
#include <stdio.h>

int
main()
{
	uint8_t code = init();
	gameLoop();
	quit(code);
}
