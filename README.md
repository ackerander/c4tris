# C4tris

Simple tetromino game written in C.

## Dependencies

libsdl2 (SDL2 libraries)

## Quick Start

```console
$ make
$ ./game
```

## Controls

- Left/Right arrows: Move left/right
- Down arrow: Soft drop
- Space: Hard drop
- Z: Rotate counter-clockwise
- X: Rotate clockwise
- C: Hold slot

## TODO

- Auto locking/placing pieces
- Game over state
- Scoring
