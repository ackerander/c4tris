OBJ = obj/main.o obj/render.o obj/game.o
DBG = obj/main.dbg.o obj/render.dbg.o obj/game.dbg.o
DEST = game
INCS = -lSDL2
CFLAGS = -std=c99 -Wall -Wextra -O3 -pedantic -mtune=native -march=native
DBGFLAGS = -std=c99 -Wall -Wextra -pedantic -ggdb
CC = cc

all: ${OBJ}
	${CC} ${INCS} -o ${DEST} ${OBJ}

debug: ${DBG}
	${CC} ${INCS} -o game.dbg ${DBG}

obj/main.o: src/main.c src/game.h
	${CC} ${CFLAGS} -c -o $@ $<

obj/render.o: src/render.c src/game.h
	${CC} ${CFLAGS} -c -o $@ $<

obj/game.o: src/game.c src/game.h
	${CC} ${CFLAGS} -c -o $@ $<

obj/main.dbg.o: src/main.c src/game.h
	${CC} ${DBGFLAGS} -c -o $@ $<

obj/render.dbg.o: src/render.c src/game.h
	${CC} ${DBGFLAGS} -c -o $@ $<

obj/game.dbg.o: src/game.c src/game.h
	${CC} ${DBGFLAGS} -c -o $@ $<

clean:
	rm ${OBJ}
